package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{


	public MergeLead clickMergeLeadFirstLookUp() {
		WebElement eleMergeLeadFirstLookUp = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(eleMergeLeadFirstLookUp);
		return this;
	}

	public MergeLead clickMergeLeadSecondLookUp() {
		WebElement eleMergeLeadSecondLookUp = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(eleMergeLeadSecondLookUp);
		return this;
	}
	public FindLeads SwitchWindowOneMergeLead() {
		switchToWindow(1);
		return new FindLeads();
	}

	public FindLeads SwitchWindowTwoMergeLead() {
		switchToWindow(2);
		return new FindLeads();
	}

	public MyLeadsPage clickMergeButton() {
		WebElement eleMergeButton = locateElement("linktext", "Merge");
		clickWithNoSnap(eleMergeButton);
		acceptAlert();
		return new MyLeadsPage();
	}



}









