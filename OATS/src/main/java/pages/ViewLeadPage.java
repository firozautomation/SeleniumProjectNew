package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	
	public EditLeadPage clickEditLead() {
		WebElement eleEditLead = locateElement("linktext", "Edit");
		click(eleEditLead);
		return new EditLeadPage();
	}
	
	public MyLeadsPage clickDeleteLead() {
		WebElement eleDeleteLead = locateElement("linktext", "Delete");
		click(eleDeleteLead);
		return new MyLeadsPage();
	}
	
	public DuplicateLead clickDuplicateLead() {
		WebElement eleDuplicateLead = locateElement("linktext", "Duplicate Lead");
		click(eleDuplicateLead);
		return new DuplicateLead();
	}
	
	
	
	
	public ViewLeadPage VerifyLeadFirstName(String cname)
	{
		WebElement eleEditCompanyName = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(eleEditCompanyName, cname) ;
		return this;
	}
	
	public ViewLeadPage VerifyLeadName(String LeadName)
	{
		WebElement eleVerifyLeadName = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(eleVerifyLeadName,LeadName) ;
		return this;
	}
	
}









