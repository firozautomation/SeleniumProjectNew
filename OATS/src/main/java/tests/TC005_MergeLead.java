package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {

	@BeforeClass
	public void setData() {
		testCaseName = "TC005_MergeLead";
		testCaseDescription ="Merge lead";
		category = "Smoke";
		author= "sainu";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public  void mergeLead(String fromLead, String toLead,String errorMag) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickMergeLead()
		.clickMergeLeadFirstLookUp()
		.SwitchWindowOneMergeLead()
		.typeFindLeadFirstName(fromLead)
		.clickFindLeadsSearch()
		.GetFirstFindLeadLinkLeadId()
		.clickMergeFirstFindLeadLink()
		.SwitchWindowTwoMergeLead()
		.typeFindLeadFirstName(toLead)
		.clickFindLeadsSearch()
		.clickMergeFirstFindLeadLink()
		.clickMergeButton()
		.clickFindLeads()
		.typeLeadID()
		.clickFindLeadsSearch()
		.VerifyMergedLeadInfo(errorMag);

	}
}