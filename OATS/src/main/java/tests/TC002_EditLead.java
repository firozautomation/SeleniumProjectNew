package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC002_Edit Lead";
		testCaseDescription ="Edit the lead informations";
		category = "Smoke";
		author= "sainu";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public  void createLead(String phoneNumber, String cname) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.ClickPhoneTab()
		.typePhoneNumber(phoneNumber)
		.clickFindLeadsSearch()
		.clickFirstFindLeadLink()
		.clickEditLead()
		.typeEditCompanyName(cname)
		.clickEditLeadUpdate()
		.VerifyLeadFirstName(cname);

	}
}
